import { Routes } from '@angular/router';

export const routes: Routes = [
  // rxjs
  { path: 'demo1', loadComponent: () => import('./features/demo1.component') },
  { path: 'demo2', loadComponent: () => import('./features/demo2.component') },
  { path: 'demo3/:postID', loadComponent: () => import('./features/demo3.component') },
  { path: 'demo4', loadComponent: () => import('./features/demo4.component') },
  { path: 'demo5', loadComponent: () => import('./features/demo5.component') },
  { path: 'demo6', loadComponent: () => import('./features/demo6.component') },
  { path: 'demo7', loadComponent: () => import('./features/demo7.component') },
  { path: 'demo8', loadComponent: () => import('./features/demo8.component') },
  { path: 'demo9', loadComponent: () => import('./features/demo9.component') },
  // reactive forms
  { path: 'demo10', loadComponent: () => import('./features/demo10.component') },
  { path: 'demo11', loadComponent: () => import('./features/demo11.component') },
  { path: 'demo12', loadComponent: () => import('./features/demo12.component') },
  { path: 'demo13', loadComponent: () => import('./features/demo13.component') },
  { path: 'demo14', loadComponent: () => import('./features/demo14.component') },
  { path: 'demo15', loadComponent: () => import('./features/demo15.component') },
  { path: 'demo16', loadComponent: () => import('./features/demo16.component') },
  { path: '', redirectTo: 'demo1', pathMatch: 'full'}
];
