import { HttpInterceptorFn } from '@angular/common/http';
import { catchError, delay, interval, of, retry, throwError } from 'rxjs';
import { environment } from '../../environments/environment';

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  let cloned = req;
  if (req.url.includes('jsonplaceholder')) {
    cloned = req.clone({
      setHeaders: {
        Authorization: 'bearer 123'
      }
    });
  }

  return next(cloned)
    .pipe(
      // delay(environment.production ? 0 : 1000),
      retry({ count: 2, delay: () => interval(1000) }),
      catchError((err) => {
        console.log('err', err)
        if (err.status === 404) {
          // redirect
        }
        return throwError(err)
      }),
      // mergeMap
    )
};
