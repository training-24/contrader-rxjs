import { HttpClient } from '@angular/common/http';
import { computed, inject, Injectable, signal } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DemoService {
  http = inject(HttpClient)
  users = signal<any[]>([])
  total = computed(() => this.users().length)

  getSomething() {
     this.http.get<any[]>('')
      .subscribe(res => {
        this.users.set(res)
      })
  }
  getSomething2() {
     return this.http.get<any[]>('')
  }
}

/*
{{srv.users()}}

// class
srv = inject(DemoService)
srv.getSomething2().subscribe(res => this.srv.users.set(res)"
*/
