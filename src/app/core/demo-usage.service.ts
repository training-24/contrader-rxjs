import { computed, inject, Injectable, signal } from '@angular/core';
import { DemoService } from './demo.service';

@Injectable({
  providedIn: 'root'
})
export class DemoUsageService {
  demoService = inject(DemoService)
  users = signal<any[]>([])
  total = computed(() => this.users().length)

  get() {
    this.demoService.getSomething2()
      .subscribe(res => {
        this.users.set(res)
      })
  }

  post() {

  }
}
