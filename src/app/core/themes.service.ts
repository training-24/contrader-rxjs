import { Injectable } from '@angular/core';
import { BehaviorSubject, interval, map, share } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemesService {
  timer$ = interval(1000).pipe(
    share()
  )
  themes$ = new BehaviorSubject<'dark' | 'light'>('dark')

  interval$ = new BehaviorSubject(0)
  interval = interval(1000)

  constructor() {
    this.interval.subscribe(this.interval$)
  }

  changeTheme(theme: 'dark' | 'light') {
    this.themes$.next(theme)
  }

  isDark() {
    return this.themes$
      .pipe(
        map(theme => theme === 'dark' )
      )
  }

}

