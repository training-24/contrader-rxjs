import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { NavigationEnd, Router, RouterLink, RouterOutlet } from '@angular/router';
import { filter, map, mergeMap } from 'rxjs';
import { ThemesService } from './core/themes.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink],
  template: `
    <!--rxjs-->
    <div class="flex gap-1 flex-wrap">
      <!--rxjs-->
      <button routerLink="demo1">Demo 1</button>
      <button routerLink="demo2">Demo 2</button>
      <button routerLink="demo3/55">Demo 3</button>
      <button routerLink="demo4">Demo 4</button>
      <button routerLink="demo5">Demo 5</button>
      <button routerLink="demo6">Demo 6</button>
      <button routerLink="demo7">Demo 7</button>
      <button routerLink="demo8">Demo 8</button>
      <button routerLink="demo9">Demo 9</button>
      
      <!--form reactive-->
      <button routerLink="demo10">Demo 10</button>
      <button routerLink="demo11">Demo 11</button>
      <button routerLink="demo12">Demo 12</button>
      <button routerLink="demo13">Demo 13</button>
      <button routerLink="demo14">Demo 14</button>
      <button routerLink="demo15">Demo 15</button>
      <button routerLink="demo16">Demo 16</button>
    </div>
    
    <button (click)="themeSrv.changeTheme('light')">light</button>
    <button (click)="themeSrv.changeTheme('dark')">dark</button>
    <hr>
    <br>

    <router-outlet />
  `,
})
export class AppComponent {
  router =inject(Router)
  http = inject(HttpClient)
  themeSrv = inject(ThemesService)

  constructor() {

    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(event => event as NavigationEnd),
       // mergeMap(event => this.http.post('https://jsonplaceholder.typicode.com/posts/', {url: event.url}))
      )
      .subscribe(res => {
          // http.get('analytics/.. url)
          // console.log(res.url)
      })


    this.themeSrv.timer$.subscribe(res => {
    //  console.log('navbar count', res)
    })
  }

}

