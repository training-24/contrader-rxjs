import { Component, inject } from '@angular/core';
import { DemoService } from '../core/demo.service';

@Component({
  selector: 'app-demo15',
  standalone: true,
  imports: [],
  template: `
    <p>
      demo15 works!
    </p>
  `,
  styles: ``
})
export default class Demo15Component {
  demoService = inject(DemoService)
}
