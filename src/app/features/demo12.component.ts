import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  ReactiveFormsModule,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-demo12',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    JsonPipe
  ],
  template: `
    <form [formGroup]="form" (submit)="save()" >
      <input type="text" placeholder="company name" formControlName="name" class="input input-bordered">
      
        <input 
          type="text" 
          [placeholder]="form.get('isCompany')?.value ? 'VAT' : 'PERSON ID'" 
          formControlName="codeId" class="input input-bordered">

      <div>
        <input type="checkbox" formControlName="isCompany"> isCompany
      </div>

      <button [disabled]="form.invalid" class="btn">Save</button>
    </form>
    
    <pre>{{form.value | json}}</pre>
    <pre>{{form.valid | json}}</pre>

  `,
  styles: ``
})
export default class Demo12Component {
  fb = inject(FormBuilder)

  form = this.fb.group({
    name: ['', [Validators.required]],
    codeId: [''],
    isCompany: true,
    pippo: 'abc'
  })

  constructor() {
    this.form.get('pippo')?.disable()

    this.form.get('isCompany')?.valueChanges
      .subscribe(isCompanyEnabled => {
        if(isCompanyEnabled) {
          this.form.get('codeId')?.setValidators([Validators.required, exactValidator(11)])
        } else {
          this.form.get('codeId')?.setValidators([exactValidator(16)])
        }
        this.form.get('codeId')?.updateValueAndValidity()
      })

    this.form.patchValue({ isCompany: true})
  }

  save() {
    console.log(this.form.value)
    // console.log(this.form.getRawValue())
  }
}









export function exactValidator(requiredLength: number): ValidatorFn {
  return (c: AbstractControl): ValidationErrors | null => {
    if (!c.value) {
      return null;
    }
    if (c.value.length === requiredLength) {
      return null;
    }
    return { codeId: true }
  }
}
/*

export function exactValidator(c: AbstractControl, requiredLength: number): ValidationErrors {
  if (c.value.length === requiredLength) {
    return null;
  }
  return { codeId: true }
}
*/
