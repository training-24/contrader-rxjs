import { AsyncPipe, JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { concatMap, map, mergeMap, of, switchMap, tap } from 'rxjs';
import { Post } from '../model/post';
import { User } from '../model/user';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [
    JsonPipe,
    RouterLink,
    AsyncPipe
  ],
  template: `
    <p>
      demo3 works!
    </p>

    <button routerLink="../44"> Post 44</button>
    <button routerLink="../55"> Post 55</button>
    <button routerLink="../66"> Post 66</button>

    <input type="text" [value]="(data$ | async)?.user?.email">
    
    
    <pre>{{ (data$ | async)?.post | json }}</pre>
    <pre>{{ (data$ | async)?.user | json }}</pre>
    
  `,
  styles: ``
})
export default  class Demo3Component {
  http = inject(HttpClient)
  activateRoute = inject(ActivatedRoute)

  data$ =  this.activateRoute.params
    .pipe(
      switchMap(params => this.http.get<Post>(`https://jsonplaceholder.typicode.com/posts/${params['postID']}`)),
      mergeMap(post => this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${post.userId}`)
        .pipe( map(user => ({ post, user }) ) )
      ),
    )

  data: {
    post: Post,
    user: User
  } | undefined;


  ngOnInit() {
    this.activateRoute.params
      .pipe(
        switchMap(params => this.http.get<Post>(`https://jsonplaceholder.typicode.com/posts/${params['postID']}`)),
        mergeMap(post => this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${post.userId}`)
            .pipe( map(user => ({ post, user }) ) )
        ),
      )
      .subscribe(res => {
         console.log(res)
      })

  }

}
