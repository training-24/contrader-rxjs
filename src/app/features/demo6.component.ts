import { log } from '@angular-devkit/build-angular/src/builders/ssr-dev-server';
import { AsyncPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { catchError, debounceTime, map, mergeMap, of } from 'rxjs';
import { Meteo } from '../model/meteo';

@Component({
  selector: 'app-demo6',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    AsyncPipe
  ],
  template: `
    <h1>Weather</h1>
    <input type="text" [formControl]="input">

  `,
  styles: ``
})
export default class Demo6Component {
  input = new FormControl('')
  http = inject(HttpClient)


  constructor() {
    // -----o---o-o-o-o-o-X
    this.input.valueChanges
      .pipe(
        debounceTime(1000),
        mergeMap(
          text => this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
            .pipe(
              catchError(() => of(null))
            )
        ),
      )
      .subscribe({
        next: result => {
          console.log(result)
        },
        error: err => console.log('error', err),
        complete: () => console.log('completed'),
      })
  }
}
