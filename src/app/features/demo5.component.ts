import { HttpClient } from '@angular/common/http';
import { Component, effect, ElementRef, inject, viewChild } from '@angular/core';
import { catchError, delay, exhaustMap, fromEvent, mergeMap, of, switchMap } from 'rxjs';

@Component({
  selector: 'app-demo5',
  standalone: true,
  imports: [],
  template: `
    <p>
      demo5 works! (see console)
    </p>
    
    <button #btn>Login</button>
    <button (click)="login()">Login2</button>
  `,
  styles: ``
})
export default  class Demo5Component {
  button = viewChild<ElementRef<HTMLButtonElement>>('btn')
  http = inject(HttpClient)

  constructor() {
    effect(() => {
      console.log(this.button()?.nativeElement)

      fromEvent(this.button()?.nativeElement!, 'click')
        .pipe(
          switchMap(
            () => this.http.get('https://jsonplaceholder.typicode.com/users2')
              .pipe(
                catchError(() => of(null))
              )
          )
        )
        .subscribe({
          next: result => console.log('result', result),
          error: error => console.log('error!!!', error),
        })
    });

  }

  login() {
    this.http.get('https://jsonplaceholder.typicode.com/users2')
      .subscribe({
        next: result => console.log('result', result),
        error: error => console.log('error!!!', error),
      })
  }

}
