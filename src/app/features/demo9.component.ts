import { AsyncPipe } from '@angular/common';
import { Component, computed, effect } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { interval, startWith } from 'rxjs';

@Component({
  selector: 'app-demo9',
  standalone: true,
  imports: [
    AsyncPipe
  ],
  template: `
    <p>
      demo9 works!
    </p>
    {{ formatTime() }}
  `,
  styles: ``
})
export default class Demo9Component {
  timer$ = interval(1000)
    .pipe(
      startWith(-1)
    )

  timer = toSignal(this.timer$)
  formatTime = computed(() => this.timer() + 'seconds')

  constructor() {
    effect(() => {
      console.log(this.timer())
    });

  }
}
