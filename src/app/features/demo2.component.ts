import { AsyncPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Post } from '../model/post';
import { User } from '../model/user';

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [
    AsyncPipe
  ],
  template: `
    @if(data$ | async; as data) {
      @for (user of data.users; track user.id) {
        <li>{{ user.name }}</li>
      }
      <hr>
      @for (post of data.posts; track post.id) {
        <li>{{ post.title }}</li>
      }
    }
   
  `,
  styles: ``
})
export default class Demo2Component {
  http = inject(HttpClient)

  data$ =  forkJoin({
    posts: this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts'),
    users: this.http.get<User[]>('https://jsonplaceholder.typicode.com/users'),
  })


}
