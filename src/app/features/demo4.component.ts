import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { combineLatest, concatMap, debounceTime, filter, forkJoin, interval, mergeMap, switchMap } from 'rxjs';
import { User } from '../model/user';

@Component({
  selector: 'app-demo4',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  template: `
    <input type="text" [formControl]="input">
    <select [formControl]="inputGender">
      <option value="male">M</option>
      <option value="female">F</option>
    </select>
    
    {{input.value}}
    {{inputGender.value}}
    
  `,
  styles: ``
})
export default class Demo4Component {
  input = new FormControl('')
  inputGender = new FormControl('')

  http = inject(HttpClient)

  constructor() {

    combineLatest([
      this.input.valueChanges,
      this.inputGender.valueChanges,
    ])
      .pipe(
       debounceTime(1000),
       // mergeMap(([text, gender]) => http.get(...))
      )
      .subscribe(res => {
        console.log(res)
      })











    return
    this.input.valueChanges
      .pipe(
        // filter(text => text!.length > 2),
        debounceTime(1000),
        mergeMap(text => this.http.get<User[]>(`https://jsonplaceholder.typicode.com/users?q=${text}`)),
      )
      .subscribe(result => {
        console.log(result)
      })
  }
}
