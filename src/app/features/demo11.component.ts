import { JsonPipe, NgClass } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { debounceTime, mergeMap } from 'rxjs';

@Component({
  selector: 'app-demo11',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    JsonPipe,
    NgClass
  ],
  template: `
    <h1>form group</h1>

    <form 
      [formGroup]="form" (submit)="save()" 
    >
      
      <input 
        type="text" class="input input-bordered "
        formControlName="name" 
        [ngClass]="{ 
          'input-error': form.get('name')?.invalid && form.dirty, 
          'input-success': form.get('name')?.valid
        }"
      >
      <input 
        type="text" formControlName="surname" class="input input-bordered"
        [ngClass]="{ 
          'input-error': form.get('surname')?.invalid && form.dirty,  
          'input-success': form.get('surname')?.valid
        }"
      >
      <button type="submit" class="btn" [disabled]="form.invalid">submit</button>
      <button type="button" (click)="reset()">reset</button>
    </form>
    
    <div>
    <pre>valid: {{form.valid}}</pre>  
    <pre>value: {{form.value | json }}</pre>  
    <pre>errors: {{form.errors | json}}</pre>  
    </div>
  `,
})
export default  class Demo11Component {
  fb = inject(FormBuilder)

  form = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(3)]],
    surname: ['', Validators.required],
  }, { updateOn: 'change'})

  constructor() {
    this.form.valueChanges
      .pipe(
        debounceTime(1000),
        // mergeMap(obj => this.http.get('', obj))
      )
      .subscribe(formValue => {
        console.log(formValue)
      })
    /*setTimeout(() => {
      const res = { name: 'Fabio', surname: 'Biondi'}
      this.form.patchValue(res)
    }, 1000)*/
  }

  reset() {
    this.form.reset()
  }
  save() {
    console.log(this.form)
  }
}

export function emailValidator() {

}
