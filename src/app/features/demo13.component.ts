import { JsonPipe, NgClass } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import {
  AbstractControl, AsyncValidatorFn,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { debounceTime, first, interval, map, mergeMap, of, take } from 'rxjs';
import { User } from '../model/user';
import { FormDetailsComponent } from '../shared/form-details.component';

@Component({
  selector: 'app-demo13',
  standalone: true,
  template: `
    <p>
      demo13 works!
    </p>

    <form [formGroup]="form" (submit)="save()">
      <!--orderID-->
      <div>
        orderid: 
        <input type="text" placeholder="order id" formControlName="orderId" class="input input-bordered">
      </div>
      <div>
        username: 
        <input type="text" placeholder="username" formControlName="username" class="input input-bordered">
        @if(form.get('username')?.pending) {
          <span class="loading loading-spinner loading-xs"></span>
        }

      </div>
      
      <h1
        class="text-3xl"
        [ngClass]="{'text-red-500': form.controls.anagrafica.invalid}"
      >Anagrafica</h1>
      <!--anagrafica-->
      <div formGroupName="anagrafica">
        <input type="text" placeholder="your name" formControlName="name" class="input input-bordered">
        <input type="text" placeholder="your surname" formControlName="surname" class="input input-bordered">
      </div>

      <!--<color-picker formControlName="color" />-->
      
      <!--passwords-->
      <h1 class="text-3xl">passwords</h1>

      @if (form.get('passwords')?.invalid) {
        <div>Don't match</div>
      }
      <div formGroupName="passwords">
        <input
          type="text" placeholder="password" formControlName="password1" class="input input-bordered"
          [ngClass]="{'input-error': form.get('passwords.password1')?.invalid}">
        <input 
          type="text" placeholder="ripeti password" formControlName="password2" class="input input-bordered"
          [ngClass]="{'input-error': form.get('passwords.password2')?.invalid}"
        >
        <pre>
        {{form.get('passwords.password2')?.errors | json}}
          </pre>
      </div>

      <button [disabled]="form.invalid || form.pending" class="btn">SAVE</button>
    </form>
    
    <pre>{{ form.value | json }}</pre>
    <pre>is valid? {{ form.valid | json }}</pre>
    
    <!--<app-form-details [data]="{name: 'fab', id: 1}" [(data)]="...."/>-->
    <pre>{{formattedData() | json}}</pre>
  `,
  imports: [
    JsonPipe,
    ReactiveFormsModule,
    NgClass,
    FormDetailsComponent
  ],
  styles: ``
})
export default class Demo13Component {
  fb = inject(FormBuilder)

  form = this.fb.group({
    orderId: [0, { validators: [Validators.required] }],
    username: ['', [Validators.required], [userValidator()]],
    anagrafica: this.fb.group({
      name: ['', [Validators.required]],
      surname: ['', Validators.required],
    }),
    passwords: this.fb.group({
      password1: ['abc', [Validators.required, Validators.minLength(3)]],
      password2: ['abc', [Validators.required, Validators.minLength(3)]],
    }, {
      validators: passwordMatch('password1', 'password2')
    })
  })

  dataForm = toSignal(this.form.valueChanges)
  formattedData = computed(() => {
    return {
      firstname: this.dataForm()?.anagrafica?.name
    }
  })

  constructor() {
    const res = {
      orderId: 123,
      anagrafica: {
        name: 'fabio',
        surname: 'biondi'
      },
    }
    this.form.patchValue(res)
  }

  save() {
    console.log(this.form.value )
    console.log(this.form.getRawValue() )
  }
}


export function userValidator(): AsyncValidatorFn {
  const http = inject(HttpClient)

  return (c: AbstractControl) => {
    return interval(1000)
      .pipe(
        take(1),
        mergeMap(() => http.get<User[]>(`https://jsonplaceholder.typicode.com/users?username=${c.value}`)
          .pipe(
            map(res => {
              return res.length ? { userExist: true } : null
            })
          ))
      )
  }

}


export function passwordMatch(p1Ref: string, p2Ref: string): ValidatorFn {
  return (g: AbstractControl): ValidationErrors | null => {
    const p1 = g.get(p1Ref);
    const p2 = g.get(p2Ref);

    if (p1?.value !== p2?.value) {
      p2?.setErrors({ ...p2?.errors, dontMatch: true}) // invalida il campo
      return { passwordsMatch: true }; // invalida il group
    } else {
      if (p2?.errors) {
        delete p2?.errors!['dontMatch']
      }
      if (p2?.errors) {
        p2?.setErrors({ ...p2?.errors })
      }
    }
    return null
  };
}
