import { AsyncPipe, JsonPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject, signal } from '@angular/core';
import { catchError, map, of, share } from 'rxjs';
import { Post } from '../model/post';
import { User } from '../model/user';

@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [
    JsonPipe,
    AsyncPipe,
    NgIf
  ],
  template: `
    <pre>{{ (posts$ |async)?.length }} posts</pre>
    
    @if(posts$ | async; as posts) {
      <pre>{{ posts.length }} posts</pre>

      @for (post of posts; track $index) {
        <li>{{ post }}</li>
      }
    }
    
    <div>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit, tempora, tenetur. Architecto assumenda autem culpa deserunt est excepturi, hic libero molestiae mollitia quisquam repellendus reprehenderit saepe temporibus tenetur ut veritatis?
    </div>
    
    <button (click)="reload()">reload</button>
    
  `,
  styles: ``
})
export default class Demo1Component {
  posts$ = inject(HttpClient)
    .get<Post[]>('https://jsonplaceholder.typicode.com/posts')
    .pipe(
      map(posts => posts.map(post => post.title)),
      catchError(err => {
        return of([])
      }),
      share()
    )

  ngOnInit() {
    this.posts$
      .subscribe(res => {
      console.log(res)
    })

  }

  reload() {
    this.posts$
      .subscribe(res => {
        console.log(res)
      })
  }


/*
  ngOnInit() {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe({
        next: data => this.usersSignal.set(data),
        error: err => console.log(err),
        complete: () => console.log('completed')
      })
  }*/

}
