import { JsonPipe } from '@angular/common';
import { Component, effect, ElementRef, signal, viewChild } from '@angular/core';
import { AbstractControl, FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ProgressMinlengthComponent } from '../shared/forms/progress-minlength.component';

const REGEX_ALPHA_NUMERIC = /^\w+$/

@Component({
  selector: 'app-demo10',
  standalone: true,
  imports: [ReactiveFormsModule, JsonPipe, ProgressMinlengthComponent],
  template: `
    
    @if(input.errors) {
      <div class="alert alert-error">
        @if(input.hasError('alphaNumeric')) { Not symbol allowed }
        @if(input.hasError('required')) { Required field }
        @if(input.hasError('minlength')) {
          Too short. Missing {{ input.getError('minlength')['requiredLength'] -input.getError('minlength')['actualLength']}} chars
        }
      </div>  
    }

    <app-progress-minlength [formInput]="input"/>
    <input type="text" [formControl]="input" class="input input-bordered">

    <button class="btn" [disabled]="input.invalid">Submit</button>
    <button class="btn" (click)="reset()">reset</button>
    
    <pre>value: {{input.value | json}}</pre>
    <pre>dirty: {{input.dirty | json}}</pre>
    <pre>touched: {{input.touched | json}}</pre>
    <pre>errors: {{input.errors | json}}</pre>
  `,
})
export default class Demo10Component {
  input = new FormControl('', {
    nonNullable: true,
    validators: [
      Validators.required, Validators.minLength(3),
      // Validators.pattern(REGEX_ALPHA_NUMERIC)
      alphaNumericValidator
    ]
  })

  reset() {
    this.input.reset()
  }
}

export function alphaNumericValidator(c: AbstractControl) {
  if (c.value && !c.value.match(REGEX_ALPHA_NUMERIC))
    return { alphaNumeric: true};

  return null
}
