import { AsyncPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import { ThemesService } from '../core/themes.service';

@Component({
  selector: 'app-demo8',
  standalone: true,
  imports: [
    AsyncPipe
  ],
  template: `
    <p
      [style.color]="(themeSrv.isDark() | async) ? 'black' : 'red'"
    >
      SUBJECT
    </p>
    {{themeSrv.themes$ | async}}
  `,
  styles: ``
})
export  default class Demo8Component {
  themeSrv = inject(ThemesService)
  sub: Subscription;

  constructor() {

    this.themeSrv.themes$.subscribe(res => {
      console.log(res)
    })
/*

    this.themeSrv.interval$.subscribe(res => {
      console.log(res)
    })
*/

    this.sub = this.themeSrv.interval$
      .subscribe(res => {
      console.log(res)
    })

    /*
    this.themeSrv.timer$.subscribe(res => {
      console.log(res)
    })*/

  }
  ngOnDestroy(): void {
    this.sub.unsubscribe()
  }
}
