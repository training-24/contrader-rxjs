import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  ValidationErrors,
  Validators
} from '@angular/forms';
import { ColorPickerComponent } from '../shared/forms/color-picker.component';
import { MyAnagraficaComponent } from '../shared/forms/my-anagrafica.component';
import { MyInputReactiveComponent } from '../shared/forms/my-input-reactive.component';
import { MyInputComponent } from '../shared/forms/my-input.component';

@Component({
  selector: 'app-demo14',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    JsonPipe,
    ColorPickerComponent,
    ColorPickerComponent,
    MyInputComponent,
    MyInputReactiveComponent,
    MyAnagraficaComponent
  ],
  template: `
    <form [formGroup]="form">
      <app-my-input-reactive 
        formControlName="name" 
        placeholder="your name" 
        label="your name"
        alphaNumeric
      />
      <app-my-input formControlName="username" placeholder="username" label="username" />
      
      <div formArrayName="items">
        @for(item of items.controls; track item) {
          <div [formGroupName]="$index">
            @if(item.valid) {
              ✅
            } @else {
              ❌
            }
            <input type="text" placeholder="name" formControlName="name"> 
            <input type="text" placeholder="cost" formControlName="cost">

            @if(items.controls.length > 1) {
            <button class="btn" (click)="removeItem($index)">DEL</button>
            }
            
            @if($last) {
            <button class="btn" (click)="addItem('', '')">ADD</button>
            }
          </div>
        }
      </div>
      
      <app-color-picker formControlName="color" />
      <app-my-anagrafica formControlName="anagrafica"></app-my-anagrafica>
      
    </form>
    
    <!--<button (click)="addItem()" class="btn btn-info">+</button>-->
    
    <pre>{{form.value | json}}</pre>
    <pre>dirty {{form.dirty | json}}</pre>
    <pre>touched {{form.touched | json}}</pre>
    <pre>valid {{form.valid | json}}</pre>
    
  `,
  styles: ``
})
export default class Demo14Component {
  fb = inject(FormBuilder)

  form = this.fb.group({
    name: ['fabio', [Validators.required]],
    // name: ['fabio', [Validators.required, alphaNumericValidator]],
    username: 'fabionski',
    color: ['red', [Validators.required]],
    items: this.fb.array([]),
    anagrafica: {
      firstname: 'fab',
      lastname: 'bio'
    }
  })

  items = this.form.get('items') as FormArray

  constructor() {
    this.addItem('', '')

    setTimeout(() => {
      this.form.patchValue({ color: 'yellow', anagrafica: { firstname: 'ciccio', lastname: 'pippo'}})
      // this.form.get('color')?.disable()
    }, 3000)
  }

  addItem(name: string, cost: string) {
    this.items.push(
      this.fb.group({
        name: [name, Validators.required],
        cost: cost
      })
    )
  }

  removeItem(index: number) {
    this.items.removeAt(index)
  }
}
export function alphaNumericValidator(c: FormControl): ValidationErrors | null {
  return (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) ?
    { alphaNumeric: true } : null
}

export const ALPHA_NUMERIC_REGEX = /^\w+$/
