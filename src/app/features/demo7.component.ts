import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject, signal } from '@angular/core';
import { catchError, concatMap, map, mergeMap, of, switchMap, toArray } from 'rxjs';
import { Todo } from '../model/todo';
import { User } from '../model/user';

@Component({
  selector: 'app-demo7',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <p>
      demo7 works!
    </p>
    @for (user of users(); track $index) {
      <div>
        <h1>{{user.profile.name}}</h1>
          @for (todo of user.todos; track todo.id) {
            <li>
              <input type="checkbox" [checked]="todo.completed"> {{todo.title}} 
            </li>
          }
      </div>
    }
    
    <pre>{{users() | json}}</pre>
  `,
  styles: ``
})
export  default class Demo7Component {
  http = inject(HttpClient)
  users = signal<{ profile: User, todos: Todo[]}[]>([])

  constructor() {
    this.http.get<User[]>(`https://jsonplaceholder.typicode.com/users`)
      .pipe(
        catchError(() => of([])),
        mergeMap(users => users),
        concatMap(
          user => this.http.get<Todo[]>(`https://jsonplaceholder.typicode.com/todos?userId=${user.id}`)
            .pipe(
              map(todos => ({
                profile: user,
                todos: todos
              }))
            )
        ),
       toArray()
       // map(user => user.name),
       // toArray()
        // map(users => users.map(u => u.name))
        // map(users => users.reduce((acc, item) => acc + item.id, 0))

      )
      .subscribe(res => {
        //const names = res.map(u => u.name)
        console.log('--->', res)
        this.users.set(res)
      })
  }
}
