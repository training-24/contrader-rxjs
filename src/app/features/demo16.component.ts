import { Component, inject } from '@angular/core';
import { DemoService } from '../core/demo.service';

@Component({
  selector: 'app-demo16',
  standalone: true,
  imports: [],
  template: `
    <p>
      demo16 works!
    </p>
  `,
  styles: ``
})
export default class Demo16Component {
  demoService = inject(DemoService)
}
