import { Component, effect, inject, input, Input, model } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { User } from '../model/user';

@Component({
  selector: 'app-form-details',
  standalone: true,
  imports: [],
  template: `
    <p>
      form-details works!
    </p>
    <form>
      <input type="text">
    </form>
  `,
  styles: ``
})
export class FormDetailsComponent {
   data = model.required<any>()
  fb = inject(FormBuilder)
  form = this.fb.group({
    name: ''
  })

  constructor() {
     this.form.valueChanges
       .subscribe(data => {
           this.data.set(data)
       })
    effect(() => {
      this.form.setValue(this.data())
    });
  }

  ngOnChanges() {
    // this.form.setValue(this.data())
  }

  /*
  @Input() set data(val: any) {
    // this.form.setValue(this.data())
  }
  */
}
