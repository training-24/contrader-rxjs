import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-my-input',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  template: `
    <div>
      <div>{{label}}</div>
      <input
        type="text"
        class="input input-bordered"
        [placeholder]="placeholder"
        [value]="value"
        (input)="onChange(ref.value)"
        #ref
      />
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting:  MyInputComponent
    }
  ]
})
export class MyInputComponent implements ControlValueAccessor{
  @Input() label: string = '';
  @Input() placeholder: string = '';
  onChange!: (color: string) => void;
  onTouch!: () => void;
  value: string = ''

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(text: string): void {
    this.value = text;
  }

}
