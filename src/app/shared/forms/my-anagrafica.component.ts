import { Component, inject } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor, FormBuilder,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, ReactiveFormsModule,
  ValidationErrors,
  Validator, Validators
} from '@angular/forms';

@Component({
  selector: 'app-my-anagrafica',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  template: `
    <form [formGroup]="form">
      <input type="text" formControlName="firstname" (blur)="onTouch()">
      <input type="text" formControlName="lastname" (blur)="onTouch()">
    </form>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: MyAnagraficaComponent
    },
    {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: MyAnagraficaComponent
    }
  ]
})
export class MyAnagraficaComponent implements ControlValueAccessor, Validator {
  fb = inject(FormBuilder)
  form = this.fb.group({
    firstname: ['', [Validators.required]],
    lastname: ['', [Validators.required]],
  })
  onTouch!: () => void;

    writeValue(obj: { firstname: string, lastname: string }): void {
        this.form.setValue(obj);
    }
    registerOnChange(fn: any): void {
       this.form.valueChanges
         .subscribe(data => fn(data))
    }
    registerOnTouched(fn: any): void {
       this.onTouch = fn;
    }

    validate(control: AbstractControl<any, any>): ValidationErrors | null {
      console.log(this.form.valid)
      // throw new Error('Method not implemented.');
      return this.form.valid ? null : { anagrafica: true};
    }
}
