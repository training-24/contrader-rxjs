import { Component, input, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-progress-minlength',
  standalone: true,
  imports: [],
  template: `
    @if(formInput().hasError('minlength')) {
      <progress
        class="progress progress-primary w-full "
        [value]="getPerc()"
        max="100"></progress>
    }
  `,
  styles: ``
})
export class ProgressMinlengthComponent {
  // @Input({ required: true }) input!: FormControl
  formInput = input.required<FormControl>()

  getPerc() {
    return (this.formInput().getError('minlength')['actualLength'] /
      this.formInput().getError('minlength')['requiredLength']) * 100
  }
}
