import { NgClass } from '@angular/common';
import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-color-picker',
  standalone: true,
  imports: [
    NgClass
  ],
  template: `
    <div class="flex gap-1">
      @for (color of colors; track color) {
        <div
          class="cell"
          [style.background-color]="color"
          [ngClass]="{
            'border-4 border-sky-400': color === value,
            'opacity-30': isDisabled
          }"
          (click)="changeColor(color)"
        ></div>
      }
    </div>
  `,
  styles: `
    .cell {
      @apply w-6 h-6 bg-slate-200
    }
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting:  ColorPickerComponent
    }
  ]
})
export class ColorPickerComponent implements ControlValueAccessor{
  @Input() colors = [
    'red', 'yellow', '#F44336', '#90CAF9', '#FFCDD2', '#69F0AE', '#AED581', '#FFE082'
  ];
  value: string = ''
  onChange!: (color: string) => void;
  onTouch!: () => void;
  isDisabled: boolean = false;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(newColor: string): void {
    this.value = newColor;
  }

  changeColor(selectedColor: string) {
    this.onTouch()
    this.value = selectedColor;
    this.onChange(selectedColor)
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
