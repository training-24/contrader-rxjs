import { JsonPipe, NgClass } from '@angular/common';
import { booleanAttribute, Component, inject, Injector, Input } from '@angular/core';
import {
    AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  NgControl,
  ReactiveFormsModule, ValidationErrors, Validator
} from '@angular/forms';
import { ALPHA_NUMERIC_REGEX } from '../../features/demo14.component';

@Component({
  selector: 'app-my-input-reactive',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    NgClass,
    JsonPipe
  ],
  template: `
    <pre>{{ngControl.errors | json}}</pre>
    @if (ngControl.errors?.['required']) {
      fiedl required
    }
    @if (ngControl.errors?.['alphaNumeric']) {
      no symbols
    }
    
    <div>
      <div>{{ label }}</div>
      <input
        [formControl]="input"
        type="text"
        class="input input-bordered"
        [ngClass]="{
          'input-error': ngControl.invalid
        }"
        [placeholder]="placeholder"
      />
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: MyInputReactiveComponent
    },
    {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: MyInputReactiveComponent
    }
  ]
})
export class MyInputReactiveComponent implements ControlValueAccessor, Validator {
  @Input({ transform: booleanAttribute }) alphaNumeric: boolean = false;
  @Input() label: string = '';
  @Input() placeholder: string = '';
  onTouch!: () => void;

  input = new FormControl()

  ngControl!: NgControl;
  injector = inject(Injector)

  ngOnInit() {
    this.ngControl = this.injector.get(NgControl)
    console.log(this.ngControl)
  }

  registerOnChange(fn: any): void {
    this.input.valueChanges
      .subscribe(data => fn(data))
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(text: string): void {
    this.input.setValue(text)
  }

  validate(c: AbstractControl<any, any>): ValidationErrors | null {
    return (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) ?
      { alphaNumeric: true } : null
  }
}
